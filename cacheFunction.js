module.exports = function(callback){
    cache = {1:1, 2:4};
    return function(args){
        if(cache[args] === undefined){
            cache[args] =  callback(args);
        }
    }
}