module.exports = function(callback, n){
    if(n === undefined || callback === undefined){
        return null;
    }else{
        return function(){
                if (n > 0){
                    callback();
                    n--;
                }
            }
    }
}